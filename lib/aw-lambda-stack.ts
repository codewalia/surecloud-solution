import * as cdk from 'aws-cdk-lib';

import * as lambda from 'aws-cdk-lib/aws-lambda';

import * as iam from 'aws-cdk-lib/aws-iam';

import { Construct } from 'constructs';

export class basicLambdaStack extends cdk.Stack {

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {

    super(scope, id, props);

    //creating an IAM role for the Lambda function
    const lambdaRole = new iam.Role(this, 'Lambda_EC2_Access_role_Role', {
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName(
          'AmazonEC2ReadOnlyAccess',
        ),
        iam.ManagedPolicy.fromAwsManagedPolicyName(
          'CloudWatchFullAccess',
        ),]
    });

    const function_name = 'get-ec2Info-lambda';

    const lambda_path = 'src/lambda';

    const handler = new lambda.Function(this, function_name, {

      functionName: function_name,

      runtime: lambda.Runtime.PYTHON_3_8,

      code: lambda.Code.fromAsset(lambda_path),

      handler: "lambda_function.lambda_handler",

      role: lambdaRole

    });

  }

}