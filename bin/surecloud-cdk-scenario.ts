#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import {SurecloudCdkScenarioPrerequisiteStack} from '../lib/surecloud-cdk-scenario-prerequisite-stack';
import {basicLambdaStack} from '../lib/aw-lambda-stack';


const app = new cdk.App();
new SurecloudCdkScenarioPrerequisiteStack(app, 'surecloud-cdk-scenario-prerequisite-stack');
new basicLambdaStack(app,'aw-lambda-stack')
