import boto3
import json

client = boto3.client('ec2')


def lambda_handler(event, context):
    try:
        #get all the isntances and show their name and tags
        response = client.describe_instances()
        result = []
        #looping through Resrevations list
        for record in response['Reservations']:
            #looping through instances list
            for instances in record['Instances']:
                #looping through tags list
                #printing all tags
                set = {}
                set['All_tags'] = instances['Tags']
                for tags in instances['Tags']:    
                    if tags['Key'] == 'Name':
                        set['Name'] = tags['Value']
                        result.append(set)
        return result
    except:
        print('There was an error, Please look into logs.')
       