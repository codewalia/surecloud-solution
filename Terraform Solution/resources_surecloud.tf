resource "aws_iam_role" "lambdaEc2AccessRole" {
  name                = "Lambda_EC2_Access_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "123456"
    }
  ]
}
EOF
managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess","arn:aws:iam::aws:policy/CloudWatchFullAccess"]
}
  

resource "aws_lambda_function" "test_lambda" {
 
  filename      = "lambda_function.py"
  function_name = "GetEc2Info"
  role          = aws_iam_role.lambdaEc2AccessRole.arn
  handler       = "lambda_handler"
  source_code_hash = filebase64sha256("lambda_function_payload.py")
  runtime = "python_3_8"

  }


