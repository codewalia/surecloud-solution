import * as cdk from 'aws-cdk-lib';
import {Template} from 'aws-cdk-lib/assertions';
import {basicLambdaStack} from '../lib/aw-lambda-stack';

test('Stack should have 1 lambda Function', () => {
    const app = new cdk.App();
    const stack = new basicLambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.resourceCountIs('AWS::Lambda::Function', 1);
});

test('Stack should have Lambda Function called get-ec2Info-lambda', () => {
    const app = new cdk.App();
    const stack = new basicLambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::Lambda::Function', {
        FunctionName: 'get-ec2Info-lambda'
    });
})

test('Stack should have Lambda Function has a Python runtime', () => {
    const app = new cdk.App();
    const stack = new basicLambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::Lambda::Function', {
        Runtime: 'python3.8'
    });
})

test('Stack should have 1 IAM role', () => {
    const app = new cdk.App();
    const stack = new basicLambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.resourceCountIs('AWS::IAM::Role', 1)
})

